﻿using Caliburn.Micro;
using MMTool.Models;
using MMTool.ViewModels;
using System;
using System.IO;
using System.Windows;

namespace MMTool
{
    public class AppBootstrapper : BootstrapperBase
    {
        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            base.OnStartup(sender, e);
            WindowManager _windowManager = new WindowManager();
            DisplayRootViewFor<ShellViewModel>();
            if (!CheckSettingDBExists())
            {
                MessageDialogViewModel messageDialogViewModel = new MessageDialogViewModel("Kein Einstellungsdatei gefunden!"
                    + Environment.NewLine
                    + "Eine neue Datei mit Standardeinstellungen wird erstellt", "Fehler");
                _windowManager.ShowDialog(messageDialogViewModel);
                using (SettingsDBContext context = new SettingsDBContext())
                {
                    var temp = new Setting()
                    {
                        SettingsName = "Example"
                    };
                    context.Settings.Add(temp);
                    context.SaveChanges();
                }
            }
        }

        private bool CheckSettingDBExists()
        {
            if (File.Exists("settings.db3"))
            {
                return true;
            }
            return false;
        }
    }
}