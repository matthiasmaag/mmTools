﻿using System.ComponentModel.DataAnnotations;

namespace MMTool.Models
{
    public class Setting
    {
        [Key]
        public int SID { get; set; }

        public string SettingsName { get; set; }
    }
}