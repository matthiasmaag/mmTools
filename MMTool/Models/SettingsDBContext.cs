﻿using System.Data.Entity;
using SQLite.CodeFirst;

namespace MMTool.Models
{
    public class SettingsDBContext : DbContext
    {
        public SettingsDBContext() : base("SettingsDBString")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteConnectionInitializer = new SqliteCreateDatabaseIfNotExists<SettingsDBContext>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);
        }

        public DbSet<Setting> Settings { get; set; }
    }
}