﻿using Caliburn.Micro;

namespace MMTool.ViewModels
{
    public class MessageDialogViewModel : Screen
    {
        private string _message;

        public MessageDialogViewModel(string message, string displayName)
        {
            TextBlock = message;
            DisplayName = displayName;
        }

        public string TextBlock
        {
            get { return _message; }
            set
            {
                _message = value;
                NotifyOfPropertyChange();
            }
        }

        public void btnOK()
        {
            this.TryClose();
        }
    }
}