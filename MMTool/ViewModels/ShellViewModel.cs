﻿using Caliburn.Micro;
using System.Collections.Generic;

namespace MMTool.ViewModels
{
    public class ShellViewModel : PropertyChangedBase
    {
        #region fields

        private Screen _currentPageViewModel;
        private List<Screen> _pageViewModels;
        private IWindowManager _windowManager;

        #endregion fields

        #region public methods

        public ShellViewModel()
        {
            _pageViewModels = new List<Screen>();
            _pageViewModels.Add(new DashboardViewModel());
            _currentPageViewModel = _pageViewModels[0];
            _windowManager = new WindowManager();
        }

        public Screen CurrentPageViewModel
        {
            get
            { return _currentPageViewModel; }
            set
            {
                _currentPageViewModel = value;
                NotifyOfPropertyChange(() => CurrentPageViewModel);
            }
        }

        #endregion public methods

        #region Eventhandler

        public bool CanDashboardBtn
        {
            get { return true; }
        }

        public void DashboardBtn()
        {
            CurrentPageViewModel = _pageViewModels.Find(x => x.DisplayName == "Dashboard");
        }

        #endregion Eventhandler
    }
}