﻿using Caliburn.Micro;
using System;

namespace MMTool.ViewModels
{
    public class DashboardViewModel : Screen
    {
        #region fields

        private IObservableCollection<string> _log;

        #endregion fields

        #region properties

        public override string DisplayName
        {
            get
            {
                return "Dashboard";
            }
        }

        public IObservableCollection<string> Log
        {
            get { return _log; }
            set
            {
                _log = value;
                NotifyOfPropertyChange();
            }
        }

        #endregion properties

        #region protected or private methods

        protected override void OnActivate()
        {
            base.OnActivate();
        }

        private void AddToLogMessage(string message)
        {
            Log.Add($"{DateTime.Now}: {message}");
            while (Log.Count > 50)
            {
                Log.RemoveAt(0);
            }
        }

        #endregion protected or private methods

        #region public methods

        public DashboardViewModel()
        {
            Log = new BindableCollection<string>();
            AddToLogMessage("Systeminformationen ermittelt");
        }

        #endregion public methods
    }
}